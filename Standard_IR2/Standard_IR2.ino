/*
Name:		Standard_IR2.ino
Created:	2/19/2016 4:34:36 PM
Author:	lhk
*/
#include "Definitions.h"
#include "Structs.h"
#include "SoundEffects.h"


#include <IRLibRecvPCI.h>
#include <IRLibSendBase.h>
#include <IRLibDecodeBase.h>
#include <IRLib_PLasertag.h>



// Objects to send and receive IR data
// unsigned int buffer[RAWBUF];
IRsendLasertag Sender;
IRrecvPCI receiver(RECVPIN);
IRdecodeLasertag decoder;

Timers timers = Timers();
PlayerState playerState = PlayerState();
Tones tones = Tones();


void setup() {
	pinMode(LEDPIN, OUTPUT);
	pinMode(RECVPIN, INPUT_PULLUP);
	pinMode(TRIGGERPIN, INPUT_PULLUP);
	pinMode(PIEZOPIN, OUTPUT);

	Serial.begin(9600);
	// delay, usually only for Leonardo
	// but who knows what these chinese arduinos are doing
	delay(500); while(!Serial); 

	receiver.enableIRIn();

	playDudel(100, 1000, 50, 30, 5);

	digitalWrite(LEDPIN, LOW);
}

void setTeam(int team)
{
	playerState.team == team;
	switch (team)
	{
		case TEAM_ONE:
			tones.shootTone=500;
			break;
	
		case TEAM_TWO:
			tones.shootTone=550;
			break;
		default:
			break;
	}
	playDots(tones.shootTone, 500, 1, 1);
}

void loop() {
	// check if we have received new IR data
	if (receiver.getResults()) {

		// three outputs:
		// 1. was the decoding successful? 0 or 1
		// 2. decoded value
		// 3. IR signal data
		
		if(decoder.decode()){

			Serial.println(decoder.value, HEX);
			decoder.dumpResults();

			int hit=3;
			if (decoder.value == hit) {
				playerState.stunned = true;
				playerState.reloading = false;
				playerState.blink = true;

				digitalWrite(LED_BUILTIN, HIGH);

				timers.generalTimer = millis();

				//300,200,700,3,stunTime
				//playSiren(500, 400, 900, 1, stunTime);// -- sounds like an ambulance
				//playKhrrek(100, 1000, 50, 30, 20);
				//playKhrrek(100, 300, 80, 10, 20); geiger
				playKhrrek(200, 300, 90, 1, 20);
				//playDots(6000, 100, 50, 3);
				
				playerState.stunned = true;
				
				digitalWrite(LED_BUILTIN, LOW);
			}
		}
		else{
			playSiren(1450, 1400, 1500, 1, Delays::invalidTimeLength/2);

			for (int i = 0; i < 5; i++) {
				digitalWrite(LED_BUILTIN, HIGH);
				delay(100);
				digitalWrite(LED_BUILTIN, LOW);
				delay(75);
			}
		}
		
		receiver.enableIRIn();
	}


	// if the player is reloading, he can't do anything
	if (playerState.reloading) {
		if (millis() - timers.reloadTimer > Delays::reloadTime) {
			playerState.reloading = false;
			timers.reloadTimer = millis();
			playDots(500, 300, 0, 1);
		}
		else if (millis() - timers.reloadStepTimer > Delays::reloadStepTime) {
			timers.reloadStepTimer = millis();
			playDots(400, 100, 0, 1);
		}
	}
	// player is neither stunned nor reloading
	else {

		// player wants to shoot
		if (digitalRead(TRIGGERPIN) == LOW) {

			// enough time has passed since last shot
			if (millis() - timers.shootTimer > Delays::shootTime) {

				timers.shootTimer = millis();

				playerState.roundsLeft--;
				if (playerState.roundsLeft <= 0)
				{
					playerState.reloading = true;
					playerState.roundsLeft = playerState.roundsMax;
					timers.reloadTimer = millis();
					timers.reloadStepTimer = millis();
				}

				// shoot
				// the only important parameter is data
				// you can send any number you want (but only numBits bits of it).
				Serial.println(millis());
				Sender.send(playerState.data);
				Serial.println(millis());
				// you can't send and receive at the same time. "shooting" automatically disables the receiver
				receiver.enableIRIn();


				for (int i = 0; i < 3; i++) {
					timers.generalTimer = millis();
					while (millis() - timers.generalTimer < Delays::shootToneLength) {

						digitalWrite(PIEZOPIN, HIGH);
						delayMicroseconds((tones.shootTone / 8) * 7);

						digitalWrite(PIEZOPIN, LOW);
						delayMicroseconds((tones.shootTone / 8));
					}
					delay(50);
				}
			}
		}
	}
}
