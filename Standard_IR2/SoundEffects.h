// SoundEffects.h

#ifndef _SOUNDEFFECTS_h
#define _SOUNDEFFECTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#error I expect arduino
#endif


// sound effects
// the note C, for reference
const int C = 1912;

// after a hit, a siren sound is being played.
// it starts with sirenStart and goes up and down with deltaSiren
struct Tones{
	const int sirenStart = C;
	const int deltaSiren = 3;
	const int deltaInvalidSiren = 8;
	const int reloadTone = 300;
	const int invalidSignal = 200;
	const int invalidTimeLength = 1000;

	int shootTone = 500; // this one depends on the team
};

void playSiren(int start, int lower, int upper, int delta, int duration);
void playDots(int frequency, int signal, int gap, int number);
void playDudel(int min, int max, int signal, int gap, int number);
void playKhrrek(int min, int max, int signal, int gap, int number);
#endif

