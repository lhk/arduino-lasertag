/* IRLib_PLSTAG_Lasertag.h
 * Part of IRLib Library for Arduino receiving, decoding, and sending
 * infrared signals. See COPYRIGHT.txt and LICENSE.txt for more information.
 */
/*
 * This is dummy code that you can copy and rename and modify when implementing new protocols.
 */

#ifndef IRLIB_PROTOCOL_LSTAG_H
#define IRLIB_PROTOCOL_LSTAG_H

#include "Definitions.h"

#define IR_SEND_PROTOCOL_LSTAG		case LSTAG: IRsendLasertag::send(data); break;
#define IR_DECODE_PROTOCOL_LSTAG	if(IRdecodeLasertag::decode()) return true;
#ifdef IRLIB_HAVE_COMBO
	#define PV_IR_DECODE_PROTOCOL_LSTAG ,public virtual IRdecodeLasertag
	#define PV_IR_SEND_PROTOCOL_LSTAG   ,public virtual IRsendLasertag
#else
	#define PV_IR_DECODE_PROTOCOL_LSTAG  public virtual IRdecodeLasertag
	#define PV_IR_SEND_PROTOCOL_LSTAG    public virtual IRsendLasertag
#endif

#ifdef IRLIBSENDBASE_H
class IRsendLasertag: public virtual IRsendBase {
  public:
    void IRsendLasertag::send(uint32_t data) {
      sendGeneric(data, numBits, 
        Head_Mark, Head_Space, 
        Mark_One, Mark_Zero, 
        Space_One, Space_Zero, 
        kHz, Use_Stop, 0);	
    };
};
#endif  //IRLIBSENDBASE_H

#ifdef IRLIBDECODEBASE_H
class IRdecodeLasertag: public virtual IRdecodeBase {
  public:
    bool IRdecodeLasertag::decode(void) {
      
      // macro for debugging information
      IRLIB_ATTEMPT_MESSAGE(F("LASERTAG_PROTOCOL"));

      // try to decode our custom protocol
      if(!decodeGeneric(numBits * 2 + 4, 
      Head_Mark, Head_Space, 
      Mark_Zero, 
      Space_One, Space_Zero)) return false;		

      protocolNum = LASERTAG_PROTOCOL;
      return true;
    }
};

#endif //IRLIBDECODEBASE_H

#define IRLIB_HAVE_COMBO

#endif //IRLIB_PROTOCOL_LSTAG_H
